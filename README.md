Godot Websockets Chat
=====

[Itch.io page](https://umarl.itch.io/godot-websockets-chat)
-----

It started with me trying to learn websockets in Godot Engine and evolved to a simple chat. [Glitch](https://glitch.com/about/) was a very nice discovery along this. Glitch is an awesome platform that lets you build apps on the web. The server itself is hosted on Glitch! Here's the project page: https://glitch.com/~godot-websockets-chat

This project was made in Godot 3.1.1 and it's based on [this example](https://github.com/godotengine/godot-demo-projects/tree/master/networking/webrtc_signaling). Since I'm new to networking, I tried making a simple chat before jumping to an actual game.

This is a learning project and things might not work as expected. Use at your own risk!!! There's more info in `node/README.md`

You can find me on twitter [@uaumarlenne](https://twitter.com/uaumarlenne)

------

[Godot Engine's website](https://godotengine.org/)

[Node.js' website](https://nodejs.org)
